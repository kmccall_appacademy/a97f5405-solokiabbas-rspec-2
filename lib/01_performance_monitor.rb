def measure(int=1)
  current=Time.now
  int.times{yield}
  if int>1
    return (Time.now-current)/int
  else
    return Time.now - current
  end
end
